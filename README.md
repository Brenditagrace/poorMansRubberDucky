Poor Man's Rubber Ducky
=======================

# Table of Contents
1. Description
2. Compatibility
3. Setup
4. Waiver

## Description
The Poor Man's Rubber Ducky is an open source automated HID (human interface device) project based on the Digispark ATtiny85 USB Development Board. A video demonstration of this tool can be seen on Vimeo: https://vimeo.com/355256663s

## Compatibility
_(**NOTE:** OSX Mojave has implemented restrictions to full disk access. The application most affected by this is Terminal. As such, the one-liner commands have been updated to reflect this.)_

Tested with OSX 10.14

## Setup
### Buying the Necessary Hardware
The Digispark ATtiny85 is an amazing little development board that can be purchased directly from Digistump or from a vendor on eBay most of which are located in China. Depending upon the market, the cost ranges from $1.30 to $8.00 per unit.
Environment Setup for Programming Within the Arduino IDE
image

1. **Go to the Arduino web site and download the Arduino IDE.** This is the IDE that you will be using to interface with the Digispark ATtiny85.
2. **Open the Arduino IDE and select File > Preferences.** Find the text field with the label "Additional Board Manager URLs" and enter http://digistump.com/package_digistump_index.json. Click the OK button when done.
3. **Under the Tools menu, select the Board submenu and then Board Manager.** The board manager dialog box will appear.
4. **Select the "Digistump AVR Boards" package and click the "Install" button.**
5. **Once the installation has completed, under the Tools menu, select the Board submenu and then "Digispark (Default 16.5mhz)" from the select list.** This is the board that we will be programming with.

### Changing the Vendor and Product Prefix to Match the Apple Keyboard
In order to have the Digispark ATtiny85 be automatically recognized as an Apple Keyboard, the development board must be flashed with correct vendor ID. To obtain the proper vendor ID for a typical Apple keyboard, do the following:

1. **Plug any wired USB Apple keyboard into a Mac.**
2. **Open the System Report on Mac.** This can be done by selecting "About This Mac" under the Apple menu and then click the "System Report…" button in the dialog window that appears.
3. **In the System Report dialog window, select "USB" in the left column.** This will show you all of the connected USB devices.
4. **Select "Apple Keyboard" under the "Keyboard Hub" section on the right.** This will show you the specifics of this device, including the Vendor ID.

Now that we have the vendor ID, we can update our usbconfig.h file so that the development board will be recognized as an Apple Keyboard.

1. **Open usbconfig.h file in your editor of choice.** Please refer to the chart below for file locations:
  * **Linux:** ~/.arduino15/packages/digistump/hardware/avr/1.6.7/libraries/DigisparkKeyboard/usbconfig.h  
  * **Macintosh:** /Applications/Arduino.app/Contents/Java/hardware/arduino/avr/bootloaders/gemma/usbconfig.h

2. **Find #define USB_CFG_VENDOR_ID in the document.** This is the configuration that you’re going to want to change. Note that the bits are entered in the reverse order that they would show up in the OSX diagnostic panes. So to update the USB_CFG_VENDOR_ID to be recognized as an Apple Keyboard, change that line to match the following:
  ```
  #define USB_CFG_VENDOR_ID 0xac, 0x05
  ```

### Modifying the Arduino script
Please note that there is at least one change that you will have to make:

1. Find This:
```
{{REMOTE_IP_ADDRESS_HERE}}
```  
2. Replace with a remote IP address which will capture the incoming TCP packets

Additionally, you may want to have a guide to modify the different actions available in the DigiKeyboard library. Since source code tends to be the best reference, you can find that here:

* **Linux:** ~/.arduino15/packages/digistump/hardware/avr/1.6.7/libraries/DigisparkKeyboard/DigiKeyboard.h
* **Macintosh:** ~/Library/Arduino15/packages/digistump/hardware/avr/1.6.7/libraries/DigisparkKeyboard/DigiKeyboard.h

### Modifying the Digispark ATtiny85 to Fit a Macbook
Unfortunately the Digispark ATtiny85 does not properly fit a Macbook USB socket. You’ll notice that when you plug it it, the least are just slightly short of where they need to be in order to make contact. However, there’s an easy fix for this. Simply take a file and file down the sides ever so slightly so that the leads are able to make a connection.

### Flashing the Board
1. **Click the Verify button in the menu bar at the top of the Arduino IDE.** In the console below, you should see output suggesting that everything is OK. If there are any issues, the Arduino IDE will alert you and highlight the suggested area.
2. **Click the Upload button in the menu bar at the top of the Arduino IDE.** If all goes well, you should see "Plug in the device now…". Plug in the device to flash the board.
3. **Once the process is complete, immediately unplug the board.** Otherwise the script will invoke and could potentially interrupt your development process.

That’s pretty much all there’s to it. There are additional customizations that you can make to these boards to further optimize their use including removing the 5 second delay. However, as of this writing I haven’t been able to create a repeatable process to make this modification yet. Once I do so, I’ll post those instructions here as well. Once again I want to give credit where credit is due and thank the Digistump Wiki whose instructions really were the backbone of this post. They have a lot of additional instructions on many of the optimizations that I just mentioned so if you don’t want to wait and feel like experimenting, I highly recommend checking out their site.

## Waiver
I have created this project as a simple, non-invasive way to remind folks to keep security at the forefront of their minds. Nothing in the script as it is being distributed will do any harm to a user's computer. That being said, USE ONLY AT YOUR OWN RISK. If you do not know what a particular Unix command will do, DO NOT USE IT. With Unix, there is NO UNDO; keep that in mind. Further, please do not blame me if this backfires or causes damage. As I just said, USE AT YOUR OWN RISK.
